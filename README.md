adflecto iOS SDK
==================================
Modified: June 8, 2016  
SDK Version: 2.3

To Download:
----------------------------------
Please go to the [Downloads](https://bitbucket.org/adflecto/adflecto-ios-sdk/downloads) section and press "**Download Repository**" button or use 

```
git clone https://artemvolftrub@bitbucket.org/adflecto/adflecto-ios-sdk.git          

```
to obtain the latest version of adflecto SDK for iOS platform.

Contains:
----------------------------------
* DemoApps - source code of a simple applications that shows how to integrate with Adflecto SDK
* Library - latest version of adflecto SDK

DemoApps:
----------------------------------
* SimpleDemo - very simple singe view app that shows how to add adflecto SDK into your code
* FlappyRect - simple "Flappy bird" clone game that shows an example of integration into real app

Getting Started with SDK:
----------------------------------
All users are recommended to review this [documentation](https://bitbucket.org/adflecto/adflecto-ios-sdk/wiki/Home) describing how to add adflecto SDK into your applictions.


Legal Requirements:
----------------------------------
By downloading adflecto SDK, you consent to comply with limited, non-commercial license to use and review the SDK - solely for evaluation purposes. If you wish to integrate this SDK into any commercial application, you must contact adflecto via email [publishers@adflecto.ru](mailto:publishers@adflecto.ru) to sign adflecto publisher agreement and become registered adflecto publisher.

Contact Us:
----------------------------------
For more information, please visit [adflecto.ru](http://adflecto.ru). In case of any question, please email us at [publishers@adflecto.ru](mailto:publishers@adflecto.ru)