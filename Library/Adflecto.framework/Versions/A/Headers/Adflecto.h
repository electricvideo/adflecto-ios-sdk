//
//  Adflecto.h
//
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 * @class Adflecto
 *
 * @discussion
 * Provides auxiliary functions for SDK.
 *
 */
@interface Adflecto : NSObject

/*!
 * @method +clearCache
 *
 * @discussion
 * Clears media file cache in background.
 */
+ (void)clearCache;

/*!
 * @method +sendLogsToServer
 *
 * @discussion
 * Sends logs to server in background.
 */
+ (void)sendLogsToServer;

/*!
 * @method +getDeviceId
 *
 * @return
 * Device AD id.
 */
+ (NSString*)getDeviceId;

/*!
 * @method +getSDKVersion
 *
 * @return
 * Adflecto SDK version.
 */
+ (NSString*)getSDKVersion;


@end
