//
// AdflectoVideoViewController.h
//
// A class that is responsible for serving interstitial video ad. Performs all ad service cycle from sending request to ad server along with processing responce, starting video player, tracking events etc.
// AD showing result is passed to AdflectoVideoViewControllerDelegate.
//
// Created by Ilya Belkin on 12.03.15.
// Copyright (c) 2015 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AdflectoDelegate.h"
#import "AdflectoAdAdapter.h"

/*!
 * @class AdflectoVideoViewController
 *
 * @discussion
 * Loads VAST XML data from backend and plays AD video.
 * AD showing result is passed to AdflectoVideoViewControllerDelegate.
 */
@class AdflectoVideoViewController;

@interface AdflectoVideoViewController : UIViewController<AdflectoAdAdapter>

/*!
* @method -initWithDelegate:withViewController:
*
* @param delegate
* delegate which receives AD showing result.
*
* @param adUnitId
* Identifier of AD placement.
*
* @param viewController
* presenting view controller.
*
* @discussion
* Designated initializer
*/
- (id)initWithDelegate:(id <AdflectoDelegate>)delegate adUnitId:(NSString*)adUnitId viewController:(UIViewController *)viewController;

/*!
 * @method -isReady
 *
 * @return
 * Returns YES if AD is ready to be shown right now.
 */
- (BOOL)isReady;

/*!
 * @method -loadAd
 *
 * @discussion
 * Loads AD data.
 */
- (void)loadAd;

/*!
 * @method -showAd
 *
 * @discussion
 * Shows AD if it is ready.
 */
-(void)showAd;

@end
