//
//  AdflectoMediator.h
//  VAST
// A class that allows to mediate with several AD sources, chooses network with best cost and shows its AD.
//  Created by Ilya Belkin on 01.02.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdflectoDelegate.h"
#import "AdflectoResult.h"

/*!
 * @class AdflectoTableViewWrapper
 *
 * @discussion
 * Allows to mediate with several AD sources, chooses network with best cost and shows its AD.
 * AD showing result is passed to AdflectoVideoViewControllerDelegate.
 */
@interface AdflectoMediator : NSObject

/*!
 * @method -initWithDelegate:withViewController:
 *
 * @param delegate
 * delegate which receives AD showing result.
 *
 * @param viewController
 * presenting view controller.
 *
 * @discussion
 * Designated initializer
 */
- (id)initWithDelegate:(id<AdflectoDelegate>)delegate viewController:(UIViewController *)viewController;

/*!
 * @method -addSource:adUnitId:floorPrice:comission
 *
 * @param source
 * AD network type.
 *
 * @param adUnitId
 * Identifier of AD placement.
 *
 * @discussion
 * Designated initializer
 */
- (void)addSource:(AdflectoAdSource)source adUnitId:(NSString*)adUnitId;

/*!
 * @method -addSource:adUnitId:floorPrice:comission
 *
 * @param source
 * AD network type.
 *
 * @param adUnitId
 * Identifier of AD placement.
 *
 * @param floorPrice
 * Floor price for AD showing.
 *
 * @param comission
 * AD showing comission.
 *
 * @discussion
 * Designated initializer
 */
- (void)addSource:(AdflectoAdSource)source adUnitId:(NSString*)adUnitId floorPrice:(NSDecimalNumber*)floorPrice comission:(NSDecimalNumber*)comission;

/*!
 * @method -isReady
 *
 * @return
 * Returns YES if at least one AD is ready to be shown right now.
 */
- (BOOL)isReady;

/*!
 * @method -loadAds
 *
 * @discussion
 * Load AD data from all provided sources.
 */
- (void)loadAds;

/*!
 * @method -showAd
 *
 * @discussion
 * Shows AD with best cost. Does nothing if no ADs are available.
 */
- (void)showAd;

/*!
 * @method -isReady:
 *
 * @param source
 * AD network type.
 *
 * @discussion
 * Returns YES if at least one AD from source is ready to be shown right now.
 */
- (BOOL)isReady:(AdflectoAdSource)source;

/*!
 * @method -getCost:
 *
 * @param source
 * AD network type.
 *
 * @discussion
 * Returns price for AD source or nil if source is not ready yet.
 */
- (NSDecimalNumber*)getCost:(AdflectoAdSource)source;

@end
