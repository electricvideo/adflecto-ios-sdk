//
//  AdflectoResult.h
//  VAST
//
//  Created by Ilya Belkin on 02.02.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AdflectoAdSource) {
    AdflectoNetworkAdSource = 0,
    MopubAdSource = 1
};

typedef NS_ENUM(NSUInteger, AdflectoResultCode) {
    /** An ad wasn't shown because of any errors */
    AdflectoError = -1,
    
    /** An ad was successfully shown and viewed until its end */
    AdflectoSuccess = 0,
    
    /** An ad showing was postponed because of slow network. Media file is downloading in a background mode */
    AdflectoPostpone = 1,
    
    /** An ad wasn't shown because of no demand */
    AdflectoNoBanner = 2,
    
    /** An ad was successfully shown and user clicked on it */
    AdflectoClick = 3,
    
    /** An ad was successfully shown but was interrupted by user */
    AdflectoClose = 4,

    /** An ad was downloaded in background and is ready to be shown */
    AdflectoReady = 5,
    
    /** User clicked "Call" button and phone call started */
    AdflectoCall = 6,
    
    /** User clicked submit button on CPA form */
    AdflectoForm = 7,

    /** AD session timeout */
    AdflectoSessionTimeout = 8

};

typedef NS_ENUM(NSUInteger, AdflectoErrorCode) {
    
    AdflectoErrorNone = 0,
    AdflectoErrorXMLParse = 1,
    AdflectoErrorSchemaValidation = 2,
    AdflectoErrorTooManyWrappers = 3,
    AdflectoErrorVideoPlayback = 4,
    AdflectoErrorNoNetwork = 5,
    AdflectoErrorPlayerNotReady = 6,
    AdflectoErrorTimeout = 7,
    AdflectoErrorPlayerHung = 8,
    AdflectoErrorMovieTooShort = 9,
    AdflectoErrorNoCompatibleMediaFile = 10,
    AdflectoErrorVastServerError = 11,
    AdflectoErrorException = 12
};

/*!
 * @class AdflectoResult
 *
 * @discussion
 * Incapsulates AD showing results.
 */
@interface AdflectoResult : NSObject

/*!
 * @property code
 *
 * @discussion
 * Resulting code.
 */
@property(nonatomic) AdflectoResultCode code;

/*!
 * @property errorCode
 *
 * @discussion
 * Resulting error code.
 */
@property(nonatomic) AdflectoErrorCode errorCode;

/*!
 * @property source
 *
 * @discussion
 * AD source network.
 */
@property(nonatomic) AdflectoAdSource source;

/*!
 * @property cost
 *
 * @discussion
 * Cost of the AD.
 */
@property(nonatomic) NSDecimalNumber* cost;

/*!
 * @property adUnitId
 *
 * @discussion
 * AD placement identifier.
 */
@property(nonatomic) NSString* adUnitId;

/*!
 * @property errorMessage
 *
 * @discussion
 * Detailed error message. Holds nil if no error occured.
 */
@property(nonatomic) NSString* errorMessage;

@end
