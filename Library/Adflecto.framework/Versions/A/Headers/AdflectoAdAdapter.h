//
//  AdflectoAdAdapter.h
//  VAST
//
//  Created by Ilya Belkin on 02.02.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

/*!
 * @protocol AdflectoAdAdapter
 *
 * @discussion
 * Protocol for interaction between AD network and AdfletoMediator.
 */
@protocol AdflectoAdAdapter <NSObject>

@required

/*!
 * @method -isReady
 *
 * @return
 * Returns YES if AD is ready to be shown right now.
 */
- (BOOL)isReady;

/*!
 * @method -loadAd
 *
 * @discussion
 * Loads AD data.
 */
- (void)loadAd;

/*!
 * @method -showAd
 *
 * @discussion
 * Shows AD if it is ready.
 */
-(void)showAd;

/*!
 * @method -getCost
 *
 * @return
 * Cost of AD showing.
 */
-(NSDecimalNumber*)getCost;

/*!
 * @method -getSource
 *
 * @return
 * Source of adapter.
 */
-(AdflectoAdSource)getSource;
@end

