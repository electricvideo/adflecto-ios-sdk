//
//  AdflectoVideoView.h
//  VAST
// A class that is responsible for serving native video ad. Performs all ad service cycle from sending request to ad server along with processing responce, starting video player, tracking events etc.
// AD showing result is passed to AdflectoVideoViewControllerDelegate.
//  Created by Ilya Belkin on 15.01.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdflectoDelegate.h"

/*!
 * @class AdflectoVideoView
 *
 * @discussion
 * Responsible for serving native video ad. 
 * Performs all ad service cycle from sending request to ad server along with processing responce, starting video player, tracking events etc.
*/
@interface AdflectoVideoView : UIView

/*!
 * @method -initWithFrame:expandable:adUnitId:viewController:delegate
 *
 * @param frame
 * Bounds and position within parent view.
 *
 * @param expandable
 * Allows switch to full-screen mode by tapping at view.
 *
 * @param autoSound
 * Defines whether an ad will be started with sound or not.
 *
 * @param adUnitId
 * Identifier of AD placement.
 *
 * @param viewController
 * presenting view controller.
 *
 * @param delegate
 * delegate which receives AD showing result.
 *
 * @discussion
 * Designated initializer
 */
- (id)initWithFrame:(CGRect)frame expandable:(BOOL)expandable autoSound:(BOOL)autoSound adUnitId:(NSString*)adUnitId viewController:(UIViewController*)viewController delegate:(id <AdflectoDelegate>)delegate;

/*!
 * @method -isReady
 *
 * @return
 * Returns YES if AD is ready to be shown right now.
 */
- (BOOL)isReady;

/*!
 * @method -loadAd
 *
 * @discussion
 * Loads AD data.
 */
- (void)loadAd;

@end
