//
//  AdflectoDelegate.h
//  VAST
//
//  Created by Ilya Belkin on 02.02.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import "AdflectoResult.h"
/*!
 * @delegate AdflectoDelegate
 *
 * @discussion
 * Delegate which receives AD showing result.
 */
@protocol AdflectoDelegate <NSObject>

@required

/*!
 * @method -adflectoFinishedWithResult:errorCode:errorMessage
 *
 * @param result
 * AD showing result
 *
 * @discussion
 * Called after AD module has finished its work.
 */
- (void)adflectoFinishedWithResult:(AdflectoResult*)result;

/*!
 * @method -adflectoVideoCachedInBackground:adUnitId:source
 *
 * @param adUnitId
 * AD unit id
 *
 * @param source
 * AD source network
 *
 * @discussion
 * Called after AD was cached and is ready to be shown.
 */
- (void)adflectoVideoCachedInBackground:(NSString*)adUnitId source:(AdflectoAdSource)source;

@optional

@end
