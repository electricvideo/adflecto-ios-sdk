//
//  main.m
//  SimpleDemo
//
//  Created by Artem on 18.05.15.
//  Copyright (c) 2015 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
