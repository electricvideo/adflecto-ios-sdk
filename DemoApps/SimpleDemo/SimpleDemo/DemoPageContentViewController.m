//
//  DemoPageContentViewController.m
//  VASTDemo
//
//  Created by Ilya Belkin on 18.01.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import "DemoPageContentViewController.h"
#import <Adflecto/AdflectoVideoView.h>

@interface DemoPageContentViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (nonatomic, strong) AdflectoVideoView* adflectoVideoView;
@end

@implementation DemoPageContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.hidesBottomBarWhenPushed = YES;
    if (self.hasVideo) {
        CGSize size = _videoContainer.bounds.size;
        CGRect frame = CGRectMake(0, 0, size.width, size.height);
        BOOL expandable = self.pageIndex == 2;
        _adflectoVideoView = [[AdflectoVideoView alloc] initWithFrame:frame expandable:expandable adUnitId:@"407a73fb3dae45f1bf7a50a07056131a" viewController:self delegate:self];
        [_videoContainer addSubview:_adflectoVideoView];
        
        self.titleLabel.text = [NSString stringWithFormat:@"%@: video page", self.titleText ];
        if (![_adflectoVideoView isReady]) {
            [_adflectoVideoView loadAd];
        }
    } else {
        self.titleLabel.text = [NSString stringWithFormat:@"%@: page without video", self.titleText ];
    }
    NSLog(@"CREATED PAGE at index %d", self.pageIndex);
}

#pragma mark - AdflectoDelegate

- (void)adflectoFinishedWithResult:(AdflectoResult*)result {
    NSLog(@"Result=%lu cost=%@ source=%lu errorCode=%lu message=%@", (unsigned long)result.code, result.cost, (unsigned long)result.source, (unsigned long)result.errorCode, result.errorMessage);
    AdflectoResultCode code = result.code;
    if (code != AdflectoPostpone && code != AdflectoReady && code != AdflectoError) {
        [_adflectoVideoView loadAd];
    }
}

- (void)adflectoVideoCachedInBackground:(NSString*)adUnitId source:(AdflectoAdSource)source {
    NSLog(@"Delegate received: video cached in background %@ %lu", adUnitId, (unsigned long)source);
}

#pragma mark - orientation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
