//
//  DemoPageContentViewController.h
//  VASTDemo
//
//  Created by Ilya Belkin on 18.01.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Adflecto/AdflectoVideoViewController.h>

@interface DemoPageContentViewController : UIViewController<AdflectoDelegate>

@property NSUInteger pageIndex;
@property NSString *titleText;
@property BOOL hasVideo;

@end
