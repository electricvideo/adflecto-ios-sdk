//
//  DemoPageViewController.m
//  VASTDemo
//
//  Created by Ilya Belkin on 18.01.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import "DemoPageViewController.h"
#import "DemoPageContentViewController.h"

#define PAGES_COUNT 10

@interface DemoPageViewController ()

@end

@implementation DemoPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dataSource = self;
    self.hidesBottomBarWhenPushed = YES;
    
    DemoPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((DemoPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((DemoPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == PAGES_COUNT) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (DemoPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (index >= PAGES_COUNT) {
        return nil;
    }
    
    DemoPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoPageContentViewController"];
    pageContentViewController.titleText = [NSString stringWithFormat:@"Page %lu", (unsigned long)index];
    pageContentViewController.pageIndex = index;
    pageContentViewController.hasVideo = (index % 2) == 0;
    
    return pageContentViewController;
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return PAGES_COUNT;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end


