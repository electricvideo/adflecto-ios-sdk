//
// CreativesTVC.m
//
// VASTDemo main application class
//
// Copyright (c) 2015 Adflecto. All rights reserved.
//

#import <Adflecto/AdflectoVideoViewController.h>
#import <Adflecto/AdflectoMediator.h>
#import <CoreLocation/CoreLocation.h>
#import "CreativesTVC.h"
#import "DemoPageViewController.h"
#import "DemoTableViewController.h"

@interface CreativesTVC () <AdflectoDelegate, CLLocationManagerDelegate  > {
    AdflectoVideoViewController *adflectoViewController;
    AdflectoMediator* mediatorSingle;
    AdflectoMediator* mediator;
    CLLocationManager *locationManager;
}
@end

@implementation CreativesTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locationManager requestWhenInUseAuthorization];
    }
    //    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    NSLog(@"%@", [locations lastObject]);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    // Cleanup vastVC if it was used before
    if (adflectoViewController) {
        adflectoViewController = nil;
    }
    
    // Load creative
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    if (cell) {
        switch (indexPath.row) {
            case 0:
            case 1: {
                if (!mediatorSingle) {
                    mediatorSingle = [[AdflectoMediator alloc]initWithDelegate:self viewController:self];
                    [mediatorSingle addSource:AdflectoNetworkAdSource adUnitId:@"407a73fb3dae45f1bf7a50a07056131a"];
                }
                [mediatorSingle loadAds];
                [mediatorSingle showAd];
                break;
            }
            case 2: {
                DemoPageViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoPageViewController"];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 3: {
                if (!mediator) {
                    mediator = [[AdflectoMediator alloc]initWithDelegate:self viewController:self];
                    [mediator addSource:AdflectoNetworkAdSource adUnitId:@"407a73fb3dae45f1bf7a50a07056131a"];
                    [mediator addSource:MopubAdSource adUnitId:@"2dd61bd62471478a9ef767346ef3dd09"];
                    [mediator addSource:MopubAdSource adUnitId:@"ec7c4b7b8ece473e802ddff002ee291b" floorPrice:[NSDecimalNumber decimalNumberWithString:@"0.15"] comission:[NSDecimalNumber zero]];
                }
                [mediator loadAds];
                if ([mediator isReady]) {
                    [mediator showAd];
                } else {
                    // continue application flow
                }
                
                break;
            }
            case 4: {
                DemoTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoTableViewController"];
                vc.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
            case 5: {
                DemoTableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoTableViewController"];
                vc.hidesBottomBarWhenPushed = NO;
                [self.navigationController pushViewController:vc animated:YES];
                break;
            }
        }
    }
}

#pragma mark - AdflectoDelegate

- (void)adflectoFinishedWithResult:(AdflectoResult*)result {
    NSLog(@"Result=%ld cost=%@ source=%ld errorCode=%ld message=%@", (long)result.code, result.cost, (long)result.source, (long)result.errorCode, result.errorMessage);
}

- (void)adflectoVideoCachedInBackground:(NSString*)adUnitId source:(AdflectoAdSource)source {
    NSLog(@"Delegate received: video cached in background %@ %ld", adUnitId, (long)source);
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
