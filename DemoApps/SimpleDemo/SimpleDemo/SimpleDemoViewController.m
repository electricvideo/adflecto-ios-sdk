//
//  ViewController.m
//  SimpleDemo
//
//  Created by Artem on 18.05.15.
//  Copyright (c) 2015 Adflecto. All rights reserved.
//

#import "SimpleDemoViewController.h"
#import "Adflecto/AdflectoDelegate.h"
#import "Adflecto/AdflectoMediator.h"

@interface SimpleDemoViewController () <AdflectoDelegate> {

    IBOutlet UITextView *resultView;
    
}

@end

@implementation SimpleDemoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSLog(@"NSStringFromUIEdgeInsets(resultView.contentInset) = %@", NSStringFromUIEdgeInsets(resultView.contentInset));
//    resultView.contentOffset = nil;
    resultView.contentOffset = CGPointMake(0.0f, 0.0f);
//    [resultView setTextAlignment:NSTextAlignmentRight];
}

- (IBAction)showAd:(id)sender {
    AdflectoMediator* mediator = [[AdflectoMediator alloc]initWithDelegate:self viewController:self];
    [mediator addSource:AdflectoNetworkAdSource adUnitId:@"407a73fb3dae45f1bf7a50a07056131a"];
    [mediator loadAds];
    if ([mediator isReady]) {
        [mediator showAd];
    } else {
        // continue application flow without AD
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)adflectoFinishedWithResult:(AdflectoResult*)result {
    NSLog(@"Result=%ld cost=%@ source=%ld errorCode=%ld message=%@", (long)result.code, result.cost, (long)result.source, (long)result.errorCode, result.errorMessage);
}

- (void)adflectoVideoCachedInBackground:(NSString*)adUnitId source:(AdflectoAdSource)source {
    NSLog(@"Delegate received: video cached in background %@ %ld", adUnitId, (long)source);
}


@end
