//
//  DemoTableViewController.m
//  VASTDemo
//
//  Created by Ilya Belkin on 10.02.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import "DemoTableViewController.h"
#import <Adflecto/AdflectoTableViewWrapper.h>

@interface DemoTableViewController() <AdflectoDelegate> {
    
}

@property (nonatomic) AdflectoTableViewWrapper* tableViewWrapper;

@end

@implementation DemoTableViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    _tableViewWrapper = [AdflectoTableViewWrapper wrapTableView:self.tableView viewController:self delegate:self adUnitId:@"407a73fb3dae45f1bf7a50a07056131a" startIndex:13 interval:10];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)dealloc {
    NSLog(@"DEALLOC - DemoTableViewController");
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 60;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88.0;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"DemoCell"];
    
    cell.textLabel.text = [NSString stringWithFormat:@"Row #%d", indexPath.row];
    cell.imageView.image = [UIImage imageNamed:@"icon"];
    cell.imageView.bounds = CGRectMake(0, 0, 30, 30);
    
    return cell;
}

#pragma mark - AdflectoDelegate

- (void)adflectoFinishedWithResult:(AdflectoResult*)result {
    NSLog(@"Result=%lu cost=%@ source=%lu errorCode=%lu message=%@", (unsigned long)result.code, result.cost, (unsigned long)result.source, (unsigned long)result.errorCode, result.errorMessage);
}

- (void)adflectoVideoCachedInBackground:(NSString*)adUnitId source:(AdflectoAdSource)source {
    NSLog(@"Delegate received: video cached in background %@ %lu", adUnitId, (unsigned long)source);
}

#pragma mark - orientation

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end
