//
//  DemoPageViewController.h
//  VASTDemo
//
//  Created by Ilya Belkin on 18.01.16.
//  Copyright © 2016 Adflecto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DemoPageViewController : UIPageViewController<UIPageViewControllerDataSource>

@end
